import React, {createContext} from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import UserStore from "./store/UserStore";
import ClothesStore from "./store/ClothesStore";
import ModalStore from "./store/ModalStore";
import OrderStore from "./store/OrderStore";

export const Context = createContext(null)

ReactDOM.render(
    <Context.Provider value={{
        user: new UserStore(),
        clothes: new ClothesStore(),
        orders: new OrderStore(),
        modal: new ModalStore()
    }}>
        <React.StrictMode>
            <App/>
        </React.StrictMode>,
    </Context.Provider>,

    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
