import React, {useState} from 'react';
import style from './RegistrationPage.module.css'
import {registration} from "../../http/userAPI";
import bgImage from "../../images/punkPage-images/PartSixFullImg.png"
import {useInput} from "../../utils/customHooks";
import Validation from "../../utilsComponents/Validation/Validation";
import Helper from "../../utilsComponents/helper/helper";



const RegistrationPage = () => {
    const [isHelper, setHelper] = useState(false)
    const [isHelperInformation, setHelperInformation] = useState('')
    const email = useInput('', {isEmpty: true, isEmail: true, minLength: 12, maxLength: 24})
    const password = useInput('', {isEmpty: true, minLength: 4, maxLength: 20})
    const login = useInput('', {isEmpty: true, minLength: 4, maxLength: 20})
    const repeatPass = useInput('')


    const signIn = async (e) => {
        try {
            await registration(email.value, password.value, login.value)
            email.setValueNull()
            password.setValueNull()
            login.setValueNull()
            repeatPass.setValueNull()
            setHelper(true)
            setHelperInformation('Account created')
            setTimeout(() => setHelper(false), 2000)

        } catch (e) {
            setHelper(true)
            setHelperInformation(e.response.data.message)
            setTimeout(()=> setHelper(false), 2000)
        }

    }

    return (
        <div className={style.wrapper} style={{backgroundImage: `url(${bgImage})`}}>
            {isHelper === true && <Helper children={isHelperInformation}/>}
            <div className={style.loginWrapper}>
                <div className={style.header}>
                    <h1>Registration a new account</h1>
                </div>
                <div className={style.form}>
                    <div>
                        <div className={style.inputWithText}>
                            <h2 className={style.textInput}>
                                Your login:
                            </h2>
                            <input type="text" placeholder='Enter your login' value={login.value}
                                   onChange={e => login.onChange(e)} onBlur={e => login.onBlur(e)}/>
                            {(login.isDirty && login.isEmpty) &&
                            <Validation children={<div>* The field cannot be empty</div>}/>}
                            {(login.isDirty && login.minLengthError) &&
                            <Validation children={<div>* Invalid length</div>}/>}
                            {(login.isDirty && login.maxLengthError) &&
                            <Validation children={<div>* Invalid length</div>}/>}
                        </div>
                        <div className={style.inputWithText}>
                            <h2 className={style.textInput}>
                                Your email:
                            </h2>
                            <input type="text" placeholder='Enter your email' value={email.value}
                                   onChange={e => email.onChange(e)} onBlur={e => email.onBlur(e)}/>
                            {(email.isDirty && email.isEmpty) &&
                            <Validation children={<div>* The field cannot be empty</div>}/>}
                            {(email.isDirty && email.minLengthError) &&
                            <Validation children={<div>* Invalid length</div>}/>}
                            {(email.isDirty && email.maxLengthError) &&
                            <Validation children={<div>* Invalid length</div>}/>}
                            {(email.isDirty && email.emailError) &&
                            <Validation children={<div>* Incorrect email</div>}/>}
                        </div>
                        <div className={style.inputWithText}>
                            <h2 className={style.textInput}>
                                Your password:
                            </h2>
                            <input type="password" placeholder='Enter your password' value={password.value}
                                   onChange={e => password.onChange(e)} onBlur={e => password.onBlur(e)}/>
                            {(password.isDirty && password.isEmpty) &&
                            <Validation children={<div>* The field cannot be empty</div>}/>}
                            {(password.isDirty && password.minLengthError) &&
                            <Validation children={<div>* Invalid length</div>}/>}
                            {(password.isDirty && password.maxLengthError) &&
                            <Validation children={<div>* Invalid length</div>}/>}
                        </div>
                        <div className={style.inputWithText}>
                            <h2 className={style.textInput}>
                                Repeat password:
                            </h2>
                            <input type="password" placeholder='Repeat password' value={repeatPass.value}
                                   onChange={e => repeatPass.onChange(e)} onBlur={e => repeatPass.onBlur(e)}/>
                            {repeatPass.value != password.value &&
                            <Validation children={<div>* Passwords don't match</div>}/>}
                        </div>
                        <div className={style.button}>
                                <button
                                    disabled={!login.inputValid || !password.inputValid || !email.inputValid}
                                    onClick={signIn}>Register
                                </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    );
};

export default RegistrationPage;