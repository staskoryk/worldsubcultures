import React from 'react'
import style from './partThreePage.module.css'


function PartThreePage(props) {
    return (
        <div className={style.containerBlock}>
            <div className={style.historySubBlockWrapper}>
                <div className={style.historyBlockHeader}>
                    <div className={style.header}>
                        <p>FANDOM.</p>
                        <p>THE EMERGENCE</p>
                        <p>OF SUBCULTURES.</p>
                    </div>
                </div>
                <div className={style.historyBlockText}>
                    <div className={style.text}>
                        <p>Fandom is a community of fans, usually of a particular subject (writer, performer, style). A fandom may have certain features of a single culture, such as" party " humor and slang, similar interests outside of the fandom, and its own publications and websites. According to some signs, fanaticism and various hobbies can acquire the features of a subculture. This, for example, happened with punk rock, gothic music, and many other interests. However, most fandoms and hobbies do not form subcultures, being focused only around the subject of their interest.</p>
                        <p>The history of subcultures dates back several decades and has three waves of rise. The first of them dates back to the 1950s. During this period, there are "stylists".</p>
                            <p>The second wave falls at the end of the 60s, the beginning of the 80s. It was influenced by both internal and external conditions. Young people of this period love music and rock ' n ' roll, drugs are used in everyday life. The new movement became deeper and longer-lasting.</p>
                            <p>The third wave of youth movements was born in 1986. Subcultures of young people are officially recognized as existing and are called "alternative".</p>
                        <div className={style.stickBlock}>
                            <div className={style.stick}>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}

export default PartThreePage