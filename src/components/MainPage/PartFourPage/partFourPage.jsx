import React from 'react'
import style from './partFourPage.module.css'
import leftPicture from '../../../images/mainPage-images/leftSideFourPagePic.png'
import rightPicture from '../../../images/mainPage-images/rightSideFourPagePic.png'

function PartFourPage(props) {
    return (
        <div className={style.containerBlock}>
            <div className={style.typeOfSubculturesWrapper}>
                <div className={style.leftSideContentBlock}>
                    <img src={leftPicture} alt="error"/>
                </div>
                <div className={style.rightSideContentBlock}>
                    <div className={style.header}>
                        <p>TYPES OF</p>
                        <p>SUBCULTURES</p>
                    </div>
                    <div className={style.text}>
                        <p>There are a huge number of different subcultures that have their own goals and lifestyles. Each type of subculture fits the thoughts of each person. Even a person who has never thought about belonging to any subculture can be similar in their thoughts and behavior to another subculture. There are so many types of subcultures that almost anyone can "calmly" say that I am a subculture.
                            In our world, there are such subcultures as: goths, punks, hippies, skinheads, hipsters, dudes, Teddy guys, emo, hackers, and so on. But each of them may have a rival subculture that is able to defend the views of its subculture, against this background there may be "skirmishes", and against this background there may be problems with the law as a result, but this is not even about this, but about the fact that the views of subcultures can be very similar or radically different. But most often, belonging to a subculture is a protest against some wrong system of the state or the daily life of people.
                        </p>
                    </div>
                    <div className={style.img}>
                        <img src={rightPicture} alt="error"/>
                    </div>
                </div>
            </div>
        </div>

    )
}

export default PartFourPage