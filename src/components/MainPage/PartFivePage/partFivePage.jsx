import React from 'react'
import style from './partFivePage.module.css'
import YouTube from "react-youtube";

// const opts = {
//     showinfo: 0,
//     height: '900',
//     width: '100%',
// }



function PartFivePage(props) {

    return (
        <div className={style.containerBlock}>
            <div className={style.videoBlock}>
                <YouTube videoId="4Kgq0miXxvQ" className={style.youtube}/>
            </div>
        </div>

    )
}



export default PartFivePage