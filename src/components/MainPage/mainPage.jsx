import React, {Suspense} from 'react'
import style from './mainPage.module.css'
import PartOnePage from "./PartOnePage/partOnePage";
import PartTwoPage from "./PartTwoPage/partTwoPage";
import PartThreePage from "./PartThreePage/partThreePage";
import PartFourPage from "./PartFourPage/partFourPage";
import PartFivePage from "./PartFivePage/partFivePage";
import PartSixPage from "./PartSixPage/partSixPage";
import Spinner from "../../utilsComponents/Spinner/Spinner";


function MainPage(props) {
    return (
        <Suspense fallback={<Spinner/>}>
            <div className={style.mainPageWrapper}>
                <PartOnePage />
                <PartTwoPage />
                <PartThreePage />
                <PartFourPage />
                <PartFivePage />
                <PartSixPage />
            </div>
        </Suspense>
    )
}

export default MainPage