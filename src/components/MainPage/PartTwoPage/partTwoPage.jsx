import React from 'react'
import style from './partTwoPage.module.css'
import bigPhoto from '../../../images/mainPage-images/bigPhoto.jpg'

function PartTwoPage(props) {
    return (
        <div className={style.containerBlock}>
            <div className={style.fullImgWrapper}>
                <img src={bigPhoto} alt="error"/>
            </div>
        </div>

    )
}

export default PartTwoPage