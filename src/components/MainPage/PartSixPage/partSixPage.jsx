import React from 'react'
import style from './partSixPage.module.css'
import bgImg from '../../../images/mainPage-images/backgroundPicBlockSix.jpg'
import rightSidePic from '../../../images/mainPage-images/rightSidePicSixPAge.png'

function PartSixPage(props) {
    return (
        <div className={style.musicBlockWrapper} style={{backgroundImage: `url(${bgImg})`}}>
            <div className={style.leftSidePage}>
                <div className={style.leftTextBlock}>
                    <div className={style.leftText}>
                        <h1>MUSIC</h1>
                        <p>Music from subcultures can be very different, and be very similar, but the genres of music in general can be absolutely anything from classical to hard heavy rock or punk. At various times, the favorites of subcultures were usually people of the time when the subculture exists, often the music was directed against the policy of the state or the music was completely unrelated to any problems, just the enjoyment of the compositions. You can learn more about music in the subcultures tabs of the app.</p>
                    </div>
                </div>
            </div>
            <div className={style.rightSidePage}>
                <img src={rightSidePic} alt="error"/>
            </div>
        </div>
    )
}

export default PartSixPage