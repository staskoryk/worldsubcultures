import React from 'react'
import style from './partOnePage.module.css'
import pictureLeft from '../../../images/mainPage-images/pictureLeft.png'
import pictureRight from '../../../images/mainPage-images/pictureRight.jpg'

function PartOnePage(props) {
    return (
        <div className={style.containerBlock}>
            <div className={style.firstPageContentWrapper}>

                <div className={style.leftSideContent}>
                    <div className={style.header}>
                        <h1 className={style.firstH1}>WORLD</h1>
                        <h1 className={style.secondH2}>SUBCULTURES</h1>
                    </div>
                    <div className={style.pictureBlock}>
                        <img src={pictureLeft} alt="ERROR"/>
                    </div>

                    <div className={style.mainText}>
                        <p className={style.textInMainText}>Subculture is a concept derived from the Latin words "sub" (under) and "cultura" (culture), and is used in sociology and cultural studies. We are talking about a part of society that differs from the vast majority in its worldview and behavior.Subculturists may differ from other people in their value systems, appearance, and other aspects.</p>
                        <p className={style.textInMainText}>If you delve into the topics of subcultures, then interest in them can attract almost every person, because each of them is imbued with some history and some roughly speaking "aesthetics".</p>
                        <p className={style.textInMainText}>Yes, to tell the truth, many types of subcultures can be "incomprehensible” to an ordinary office "worm", but in any case, for personal knowledge, such a topic seems to me to be interesting.</p>
                    </div>

                </div>
                <div className={style.rightSideContent}>
                    <div className={style.blockWithQuote}>
                        <p>“Often people punch themselves in the chest, declaring their involvement in the subculture or underground, and it is worth feeding them a little, their views change dramatically, it all starts: he was young, stupid, and so on…”</p>
                    </div>
                    <div>
                        <img src={pictureRight} className={style.rightPicture} alt="error"/>
                    </div>
                </div>
            </div>
        </div>

    )
}

export default PartOnePage