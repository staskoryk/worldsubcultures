import React from 'react';
import style from './PartNinePunkPage.module.css'
import sexpistols from "../../../images/punkPage-images/sexpistols.png";

const PartNinePunkPage = () => {
    return (
        <div className={style.containerBlock}>
            <div className={style.wrapper}>
                <div className={style.letovImg}>
                    <img src={sexpistols} alt=""/>
                </div>
                <div className={style.header}><h2>SEX PISTOLS - representatives of foreign culture</h2></div>
            </div>
        </div>

    );
};

export default PartNinePunkPage;