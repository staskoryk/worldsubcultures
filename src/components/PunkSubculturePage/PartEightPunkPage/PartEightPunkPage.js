import React from 'react';
import style from './PartEightPunkPage.module.css'
import letov from '../../../images/punkPage-images/letov.png'

const PartEightPunkPage = () => {
    return (
        <div className={style.containerBlock}>
            <div className={style.wrapper}>
                <div className={style.letovImg}>
                    <img src={letov} alt=""/>
                </div>
                <div className={style.header}><h2>EGOR LETOV - representatives of foreign punk</h2></div>
            </div>
        </div>

    );
};

export default PartEightPunkPage;