import React from 'react'
import style from './PunkSubculturePage.module.css'
import PartOnePunkHistoryPage from "./PartOnePunkHistoryPage/PartOnePunkHistoryPage";
import PartTwoPunkPage from "./PartTwoPunkPage.jxs/PartTwoPunkPage";
import PartThreePunkPage from "./PartThreePunkPage/PartThreePunkPage";
import PartFourPunkPage from "./PartFourPunkPage/PartFourPunkPage";
import PartFivePunkPage from "./PartFivePunkPage/PartFivePunkPage";
import PartSixPunkPage from "./PartSixPunkPage/PartSixPunkPage";
import PartSevenPunkPage from "./PartSevenPunkPage/PartSevenPunkPage";
import PartEightPunkPage from "./PartEightPunkPage/PartEightPunkPage";
import PartNinePunkPage from "./PartNinePunkPage/PartNinePunkPage";


function PunkSubculturePage(props) {
    return (
        <div> className={style.mainPageWrapper}>
            <PartOnePunkHistoryPage/>
            <PartTwoPunkPage/>
            <PartThreePunkPage/>
            <PartFourPunkPage/>
            <PartFivePunkPage/>
            <PartSixPunkPage/>
            <PartSevenPunkPage/>
            <PartEightPunkPage/>
            <PartNinePunkPage/>
        </div>

    )
}

export default PunkSubculturePage