import React from 'react';
import style from './PartFourPunkPage.module.css'
import imageMiddle from '../../../images/punkPage-images/partFourPunkPageMIddle.png'

import Li from "../../../utilsComponents/LI";

const itemsLeft = [
    {
        name: 'Mohawk',
        description: 'The hairstyle got its name thanks to the Iroquois - a group of American Indian tribes. It is believed that the first to put hair in this way among punks was the vocalist of the band The Exploited Watty Byoken'
    },
    {
        name: 'Vocabulary',
        description: 'Punks often used harsh words, thus expressing their contempt for the norms of public morality.'
    },
    {
        name: 'Pins',
        description: 'Pins were popularized by Sex Pistols vocalist Johnny Rotten, who thus "repaired" torn clothes'
    },
    {
        name: 'Anarchy',
        description: 'The sign of anarchy is one of the most popular tattoos among punks, they considered complete lawlessness to be the ideal state structure'
    },
    {
        name: 'Chains',
        description: 'Sex Pistols bassist Sid Vicious wore a small lock on a chain around his neck, later many punks began to wear the same accessory'
    },
    {
        name: 'Leather jacket',
        description: 'A leather jacket with a slant of zippers is good because it can not be set on fire with a cigarette or get dirty. Even after falling into a ditch, the punk can easily remove all the dirt with a few movements with a damp cloth'
    },
    {
        name: 'Drugs and Alcohol',
        description: 'The song “Sex & Drugs & Rock\'n\'roll”by Ian Durry became a kind of punk anthem. In this subculture, it was believed that harmful pleasures were worth dying young'
    },
]

const itemsRight = [

    {
        name: 'Rivets',
        description: 'Leather jackets were decorated with various rivets and spikes. The latter warned other members of the society that it was better not to approach punks'
    },
    {
        name: 'Nazi paraphernalia',
        description: 'The punks were staunch anti-fascists, which, however, did not prevent them from using Nazi symbols like Sid Vicious. It was done for kitsch'
    },
    {
        name: 'Anti-war symbols',
        description: 'They opposed any war, and did not serve in the army on principle. True, if the hippies were pacifists out of love for the world and all the people on Earth, then the punks simply did not want to become cannon fodder for the class of those in power'
    },
    {
        name: 'Pogo',
        description: 'Sid Vicious said he was the one who invented pogo. During the performance of this peculiar dance, punks with a straight back bounced energetically on the spot, pressing their hands to their bodies'
    },
    {
        name: 'Ripped jeans',
        description: 'The ostentatiously casual style of dress was intended to demonstrate a challenge to society and its norms'
    },
    {
        name: 'Tackles',
        description: 'In order to better show the high army boots to others, the punks rolled up their jeans'
    },
    {
        name: 'Red shoelaces',
        description: 'One of the most common symbols of anti-fascism'
    },
]

const PartFourPunkPage = () => {


    return (
        <div className={style.containerBlock}>
            <div className={style.partFourPageBlock}>
                <div className={style.leftSideBlock}>
                    <ol>
                        {itemsLeft.map(item => <Li children={item.name} description={item.description}/>)}
                    </ol>
                </div>
                <div className={style.middleImgBlock}>
                    <img src={imageMiddle} alt="error"/>
                </div>
                <div className={style.rightSideBlock}>
                    <ol>
                        {itemsRight.map(item => <Li children={item.name} description={item.description}/>)}
                    </ol>
                </div>
            </div>
        </div>
    );
};

export default PartFourPunkPage;