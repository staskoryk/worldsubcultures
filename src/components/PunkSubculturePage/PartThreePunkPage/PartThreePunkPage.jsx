import React from 'react';
import style from './PartThreePunkPage.module.css'

import leftImg from '../../../images/punkPage-images/partThreePunkPageLeft.png'
import middleImg from  '../../../images/punkPage-images/partThreePunkPageMiddle.png'
import rightPage from '../../../images/punkPage-images/partThreePunkPageRight.png'

const PartThreePunkPage = () => {
    return (
        <div className={style.containerBlock}>
            <div className={style.partThreePageWrapper}>
                <div className={style.leftSideBlock}>
                    <div className={style.headerLeftSide}>
                        <p>"PUNKS HOI" AND ANARCHY.</p>
                        <p>OR ABOUT THE IDEALOGY OF PUNKS</p>
                    </div>
                    <div className={style.textLeftSide}>
                        <p>It is difficult to single out a single ideology among the punks, because as a social construct they developed, divided, and many directions appeared. But, in general, they are united by nonconformism, the desire for personal freedom and independence, the promotion of DIY principles (do it yourself) and contempt for commerce. For example, it's not cool to buy a patch on a jacket, it's cool to make it yourself from "shit and sticks".</p>
                    </div>
                    <div className={style.imgLeftSide}>
                        <img src={leftImg} alt="error"/>
                    </div>
                </div>
                <div className={style.middleSideBlock}>
                    <div className={style.middleImg}>
                        <img src={middleImg} alt="error"/>
                    </div>
                    <div className={style.middleText}>
                        <p>Protest plays an important role in the life of punk. It can be cultural, disregarding the rules of morality and aesthetics, or social, fighting for the rights of people, or it can be anarchist, opposing the state system and public institutions.</p>
                        <p>The main value from the point of view of punk is freedom. And it should be in everything. No one should impose anything.</p>
                    </div>
                </div>
                <div className={style.rightSideBlock}>
                    <div className={style.rightImg}>
                        <img src={rightPage} alt="error"/>
                    </div>
                    <div className={style.rightText}>
                        <p>Among the trends in the politics of punks, nihilism, anarchism, and socialism are distinguished.</p>
                        <p>Anarchism is an ideology of anarchy. Anarchism presupposes the absence of any power in society, including state power.</p>
                        <p>Anarchists seek to destroy all forms of exploitation of people by each other. Instead, they propose to establish the cooperation of individuals, so that social relations and social institutions are built on the personal interest of all their participants.</p>
                        <p>Anarchists, along with socialists and communists, are considered representatives of the left ideology, contrasting them with the right-liberals and conservatives.</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default PartThreePunkPage;