import React from 'react';
import style from './PartSixPunkPage.module.css'
import fullImg from '../../../images/punkPage-images/PartSixFullImg.png'

const PartSixPunkPage = () => {
    return (
        <div className={style.containerBlock}>
            <div className={style.fullImg} style={{backgroundImage: `url(${fullImg})`}}>

            </div>
        </div>
    );
};

export default PartSixPunkPage;