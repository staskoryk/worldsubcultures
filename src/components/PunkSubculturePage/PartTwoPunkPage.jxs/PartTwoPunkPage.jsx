import React from 'react';
import style from './PartTwoPunkPage.module.css'
import paperPic from '../../../images/punkPage-images/punksPaper.png'

const PartTwoPunkPage = () => {
    return (
        <div className={style.containerBlock}>
            <div className={style.partTwoPageWrapper}>
                <img src={paperPic} alt="error"/>
            </div>
        </div>
    );
};

export default PartTwoPunkPage;