import React from 'react';
import style from './PartFivePunkPage.module.css'
import topPartOnePic from '../../../images/punkPage-images/PartFivePunkPage/OnePartTopPic.png'
import bottomPartOnePic from '../../../images/punkPage-images/PartFivePunkPage/PartOneBottomPic.png'
import topPartTwoPic from '../../../images/punkPage-images/PartFivePunkPage/PartTwoTopPic.png'
import bottomPartTwoPic from '../../../images/punkPage-images/PartFivePunkPage/PartTwoBottomPic.png'
import topPartThreePic from '../../../images/punkPage-images/PartFivePunkPage/PartThreeTopPic.png'
import bottomPartThreePic from '../../../images/punkPage-images/PartFivePunkPage/PartThreeBottomPic.png'
import topPartFourPic from '../../../images/punkPage-images/PartFivePunkPage/PartFourTopPic.png'
import bottomPartFourPic from '../../../images/punkPage-images/PartFivePunkPage/PartFourBottomPic.png'
import topPartFivePic from '../../../images/punkPage-images/PartFivePunkPage/PartFiveTopPic.png'
import bottomPartFivePic from '../../../images/punkPage-images/PartFivePunkPage/PartFiveBottomPic.png'

const PartFivePunkPage = () => {
    return (
        <div className={style.containerBlock}>
            <div className={style.partFivePunkPageWrapper}>
                <div className={style.onePartPage}>
                    <div className={style.topOnePartPage}>
                        <img src={topPartOnePic} alt="error"/>
                    </div>
                    <div className={style.middleOnePartPage}>
                        <p>Punk rock - dynamic, hard guitar music. The luminaries of this genre are the groups "Exploited" and " Sex Pistols"</p>
                    </div>
                    <div className={style.bottomOnePartPage}>
                        <img src={bottomPartOnePic} alt="error"/>
                    </div>
                </div>
                <div className={style.twoPartPage}>
                    <div className={style.topTwoPartPage}>
                        <img src={topPartTwoPic} alt="error"/>
                    </div>
                    <div className={style.middleTwoPartPage}>
                        <p>Pop-punk is a milder form of punk rock. Fast, tough, not politically correct, but quite life-affirming. (Example group " Ramones")</p>
                    </div>
                    <div className={style.bottomTwoPartPage}>
                        <img src={bottomPartTwoPic} alt="error"/>
                    </div>
                </div>
                <div className={style.threePartPage}>
                    <div className={style.topThreePartPage}>
                        <img src={topPartThreePic} alt="error"/>
                    </div>
                    <div className={style.middleThreePartPage}>
                        <p>The most radical and aggressive branch of punk is OI. The lyrics are politicized, the music is harsh, vicious. Musicians strive to prove their affinity for the proletariat. The most famous bands are: "Blitz", "Cock Sparrer", "Business" and others.</p>
                    </div>
                    <div className={style.bottomThreePartPage}>
                        <img src={bottomPartThreePic} alt="error"/>
                    </div>
                </div>
                <div className={style.fourPartPage}>
                    <div className={style.topFourPartPage}>
                        <img src={topPartFourPic} alt="error"/>
                    </div>
                    <div className={style.middleFourPartPage}>
                        <p>Punk-hardcore (Examples of the band "Circle Jerks" and " Black Flag")</p>
                    </div>
                    <div className={style.bottomFourPartPage}>
                        <img src={bottomPartFourPic} alt="error"/>
                    </div>
                </div>
                <div className={style.fivePartPage}>
                    <div className={style.topFivePartPage}>
                        <img src={topPartFivePic} alt="error"/>
                    </div>
                    <div className={style.middleFivePartPage}>
                        <p>Sycobilly - appeared in the early 80's thundering mix of punk rock with lyrical melody and psychedelic elements.</p>
                    </div>
                    <div className={style.bottomFivePartPage}>
                        <img src={bottomPartFivePic} alt="error"/>
                    </div>
                </div>

            </div>
        </div>
    );
};

export default PartFivePunkPage;