import React from 'react';
import style from './PartSevenPunkPage.module.css'
import YouTube from "react-youtube";

const PartSevenPunkPage = () => {
    return (
        <div className={style.containerBlock}>
            <div className={style.partSevenBlock}>
                <YouTube videoId="ox001BX4cmc" className={style.youtube}/>
            </div>

        </div>
    );
};

export default PartSevenPunkPage;