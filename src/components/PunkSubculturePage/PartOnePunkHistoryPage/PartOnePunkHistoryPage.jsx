import React from 'react';
import style from './PartOneHistoryPage.module.css'
import punkPage from  '../../../images/punkPage-images/punkOnePartPage.png'

const PartOnePunkHistoryPage = (props) => {
    return (
        <div className={style.containerBlock}>
            <div className={style.partOneWrapper}>
                <div className={style.punkBlockPic}>
                    <img src={punkPage} alt="error"/>
                </div>
                <div className={style.punkBlockText}>
                    <div className={style.headerNameBlock}>
                        <span>History of the Punks</span>
                    </div>
                    <div className={style.blockWithText}>
                        <p>Punks are a musical and deeply politicized subculture. They appeared during the decline of the economy of Britain and the United States. Working-class youth could no longer afford beautiful clothes, stories, and an idle lifestyle. Rock ' n ' Roll has fallen apart and caved in to commerce. All those sleek, shaggy boys and glittering stars were downright infuriating. Thus was born the punk who protested against glamour, venality, and, in the end, the state.</p>

                            <p>It is believed that punk rock originated from garage rock in the 1960s. He then remained in the niche of local underground scenes and avoided contact with the mainstream. But nothing lasts forever and the genre eventually sold out to major labels. And those who did not sell out, for example," The Stooges " marked the beginning of punk rock.</p>

                            <p>As a result, after a long journey of transformation, two significant bands played vividly on the music scene. They were the Ramones in New York and the Sex Pistols in London. Against their background, the whole movement turned into a subculture. The key features were the protest ideology, distinctive style of clothing and jewelry.</p>
                    </div>
                </div>

            </div>
        </div>

    );
};

export default PartOnePunkHistoryPage;