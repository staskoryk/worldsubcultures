import React from 'react';
import style from './PartFourSkinheadPage.module.css'
import imgLeft from '../../../images/skinhead-images/imgLeftPartFour.png'
import Li from "../../../utilsComponents/LI";

const items = [
    {
        name: 'SHAVED HEAD',
        description: 'Skins often participated in street fights, in which one of the easiest ways to immobilize an opponent and harm him is to grab him by the hair'
    },
    {
        name: 'BOMBER JACKET',
        description: 'They were very comfortable to wear, did not restrict movement and at the same time looked stylish. That is why they were loved by football fans, and then by skinheads'
    },
    {
        name: 'LEVI\'S 501 JEANS',
        description: 'Just stylish jeans, camouflage pants or regular pants were also worn'
    },
    {
        name: 'TACKLES',
        description: 'To make the surrounding people better see the high army boots, the skins were tucked up jeans'
    },
    {
        name: 'SUSPENDERS',
        description: 'The fashion to carry skin traction began in the 60s, when working jeans did not have belt loops and workers had to wear suspenders to avoid losing their pants'
    },
    {
        name: 'DR.MARTENS SHOES',
        description: 'More comfortable than army boots and look more stylish'
    },
    {
        name: 'TATTOOS',
        description: 'Symbolism of the 3rd Reich'
    },
]

const PartFourSkinheadPage = () => {
    return (
        <div className={style.block}>
            <div className={style.left}>
                <img src={imgLeft} alt=""/>
            </div>
            <div className={style.right}>
                <ol>
                    {items.map(item => <Li children={item.name} description={item.description}/>)}
                </ol>
            </div>
        </div>
    );
};

export default PartFourSkinheadPage;