import React from 'react';
import style from './SkinheadSubculturePage.module.css'
import PartOneSkinheadPage from "./PartOneSkinheadPage/PartOneSkinheadPage";
import PartTwoSkinheadPage from "./PartTwoSkinheadPage/PartTwoSkinheadPage";
import PartThreeSkinheadPage from "./PartThreeSkinheadPage/PartThreeSkinheadPage";
import PartFourSkinheadPage from "./PartFourSkinheadPage/PartFourSkinheadPage";
import PartFiveSkinheadPage from "./PartFiveSkinheadPage/PartFiveSkinheadPage";

const SkinheadSubculturePage = () => {
    return (
        <div className={style.skinheadWrapper}>
            <PartOneSkinheadPage/>
            <PartTwoSkinheadPage/>
            <PartThreeSkinheadPage/>
            <PartFourSkinheadPage/>
            <PartFiveSkinheadPage/>
        </div>
    );
};

export default SkinheadSubculturePage;