import React from 'react';
import style from './PartOneSkinheadPage.module.css'
import wnl from '../../../images/skinhead-images/imgWNL.png'

const PartOneSkinheadPage = () => {
    return (
        <div className={style.block}>
            <img src={wnl} alt=""/>
        </div>
    );
};

export default PartOneSkinheadPage;