import React from 'react';
import style from './PartTwoSkinheadPage.module.css'
import history from '../../../images/skinhead-images/history.png'


const PartTwoSkinheadPage = () => {
    return (
        <div className={style.block}>
            <img src={history} alt="history"/>
            <div className={style.blockWithText}>
                <h1>HISTORY</h1>
                <p>The skinhead subculture is radically different from any other culture that has emerged under the influence of music. It all started, of course, in England, in good old London. So, the movement of skinheads (skinheads, leather heads – eng.) took off from the 60s of the twentieth century in poor working-class neighborhoods. And it came from the very popular movement of mods (modernist, or, as they were also called, stylists), the movement of teddy boys (and in Russian gopnikov) and football hooligans.</p>
                <p>The first skins respected blacks and mulattoes. No wonder – there were many immigrants among the workers of that time. Skins and visitors from Jamaica had a common view, listened to the same music, in particular reggae and ska. A very big influence on the skin movement was the current of football hooligans. In many ways, it is to him that the skins are due to the bomber jackets, which made it easy to slip out of the hands of an opponent in a street brawl, a shaved head, thanks to which it was impossible to grab the bully by the hair. Of course, the skin youth had a lot of trouble with the police. Tellingly, both boys and girls participated in the movement. It is not superfluous to note that, like all football fans, skinheads liked to spend time in the pub for a glass of foam.</p>
            </div>
        </div>
    );
};

export default PartTwoSkinheadPage;