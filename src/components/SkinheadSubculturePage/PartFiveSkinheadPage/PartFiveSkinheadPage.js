import React from 'react';
import style from './PartFiveSkinheadPage.module.css'

import img1 from '../../../images/skinhead-images/imgOnePartFive.png'
import img2 from '../../../images/skinhead-images/imgTwoPartFive.png'
import img3 from '../../../images/skinhead-images/imgThreePartFive.png'

const PartFiveSkinheadPage = () => {
    return (
        <div className={style.block}>
            <div className={style.left}>
                <img src={img1} alt=""/>
            </div>
            <div className={style.right}>
                <div className={style.topImg}>
                    <img src={img2} alt=""/>
                </div>
                <div className={style.bottomImg}>
                    <img src={img3} alt=""/>
                </div>
            </div>
        </div>
    );
};

export default PartFiveSkinheadPage;