import React from 'react';
import style from './PartThreeSkinheadPage.module.css'
import mid from '../../../images/skinhead-images/imgMIDpartThree.png'
import leftImg from '../../../images/skinhead-images/imgLeftpartThree.png'
import rightImg from '../../../images/skinhead-images/imgRightpartThree.png'

const PartThreeSkinheadPage = () => {
    return (
        <div className={style.block}>
            <div className={style.left}>
                <div className={style.leftText}>
                    <h1>ABOUT THE IDEOLOGY OF SKINHEADS</h1>
                    <p>Racist skinheads are the scourge of modern society. They constantly arrange fights, beatings of
                        foreign citizens, protest actions. They are arrested, convicted, imprisoned, but they remain
                        true to their ideals. The idea is simple – the superiority of the white race and the
                        purification of the country from alien elements.</p>
                    <p>Taking advantage of the popular dislike of foreigners, skinheads often recruit an impressive
                        number of young people into their ranks. In Russia, the Nazi-skinhead movement is popular to the
                        point of outrage.</p>
                    <p>Recently, it has come to the point that foreigners are simply afraid to be in the country and
                        prefer to live where the problem of Nazism is not so acute. On the one hand,
                        the Nazi ideology seems cruel and inhumane.</p>
                </div>
                <div className={style.leftImg}>
                    <img src={leftImg} alt=""/>
                </div>
            </div>
            <div className={style.middle}>
                <img src={mid} alt=""/>
            </div>
            <div className={style.right}>
                <div className={style.rightImg}>
                    <img src={rightImg} alt=""/>
                </div>
                <div className={style.rightText}>
                    <p>The actions of skins find a huge resonance in modern society – they are hated, despised, tried to catch and punish. Killing people is certainly not a good thing to do.
                    </p>
                    <p>On the other hand, it is impossible not to notice that the actions of the skinheads had an effect-foreigners
                        they do not feel as free in the country as they used to. Objectively, we can say that skinheads
                        are a way to protect society from excessively arrogant immigrants.</p>
                    <p>However, it is a pity that the murders of blacks and other citizens are often unjustified and do not bear the character
                        of retribution, which could be explained. The actions of Russian skins are usually an attack on innocent
                        black students, entrepreneurs, and so on.</p>
                </div>
            </div>
        </div>
    );
};

export default PartThreeSkinheadPage;