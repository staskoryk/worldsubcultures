import React, {useContext, useEffect} from 'react';
import style from './FavoriteClothesPage.module.css'
import {NavLink} from "react-router-dom";
import {SHOP_ROUTE} from "../../utils/consts";
import touchFav from "../../images/favorite-image/touchFav.png"
import {observer} from "mobx-react-lite";
import {Context} from "../../index";
import OneFavClothes from "./OneFavClothes/OneFavClothes";
import {fetchFavorites} from "../../http/userAPI";




const FavoriteClothesPage = observer(() => {

    const {user} = useContext(Context)

    useEffect(() => {
        fetchFavorites(user.user.id).then(data => user.setFavoriteClothes(data))
    })

    return (
        <div className={style.favoriteWrapper}>
            {user.favoriteClothes.length === 0
                ?
                <div className={style.wrapperEmpty}>
                    <img className={style.imgFav} src={touchFav} alt=""/>
                    <div className={style.header}>
                        <span>Favorite is empty</span>
                    </div>
                    <div className={style.text}>
                        <span>To select items, go to the catalog</span>
                    </div>
                    <div className={style.button}>
                        <NavLink to={SHOP_ROUTE}>
                            <button>Go to the catalog</button>
                        </NavLink>
                    </div>
                </div>
                :
                <>
                    <div className={style.favoritesHeader}>
                        <span>Favorites clothes</span>
                    </div>
                    <div className={style.wrapperNotEmpty}>
                        <div className={style.favItems}>
                            {user.favoriteClothes.map(item =>
                                <OneFavClothes item={item}/>
                            )}

                        </div>
                    </div>
                </>

            }

        </div>
    );
});

export default FavoriteClothesPage;