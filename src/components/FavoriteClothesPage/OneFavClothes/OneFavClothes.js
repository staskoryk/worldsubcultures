import React, {useContext, useState} from 'react';
import style from "../FavoriteClothesPage.module.css";
import unlike from "../../../images/shopPage-images/unLike.png";
import like from "../../../images/shopPage-images/like.png";
import {useHistory} from 'react-router-dom'
import {observer} from "mobx-react-lite";
import {Context} from "../../../index";
import {addItemToBasket, deleteInFavorite} from "../../../http/userAPI";
import {CLOTHES_ROUTE} from "../../../utils/consts";

const OneFavClothes = observer(({item}) => {

    const history = useHistory()
    const {user} = useContext(Context)

    const deleteFavoriteClothes = async() => {
        try {
            await deleteInFavorite(user.user.id, item._id)
        } catch (e) {
            alert(e.response.data.message)
        }
    }

    const addToBasket = async () => {
        try {
            await addItemToBasket(user.user.id, item._id)
        } catch (e) {
            alert(e.response.data.message)
        }
    }

    const [likeImg, setLikeImg] = useState(false)
    return (
        <div className={style.favItem} id={item.id}>
            <div className={style.imgFavImg}>
                <img src={item.imageSrc} alt="" onClick={() => history.push(CLOTHES_ROUTE + '/' + item._id)}/>
                <div className={style.like}>
                    {likeImg ?
                        <div><img src={unlike} alt="" onClick={()=>setLikeImg(false)}/></div>
                        :
                        <div onClick={deleteFavoriteClothes}><img src={like} alt="" onClick={()=>setLikeImg(true)}/></div>
                    }
                </div>
            </div>
            <div className={style.infoFavItem}>
                <div className={style.infoFavItemHeader}>
                    <span>{item.name}</span>
                </div>
                <div className={style.infoFavItemInfo}>
                    <span>Price: {item.price}$</span>
                </div>
                <div className={style.infoFavItemButton}>
                    <button onClick={addToBasket}>Add to basket</button>
                </div>
            </div>
        </div>
    );
});

export default OneFavClothes;