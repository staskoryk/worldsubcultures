import React, {useContext} from 'react';
import style from './ListItemsLogin.module.css'
import {NavLink} from "react-router-dom";
import {observer} from "mobx-react-lite";
import {Context} from "../../../index";
import {BASKET_ROUTE, CHAT_ROUTE, FAVORITE_ROUTE, ORDER_ROUTE} from "../../../utils/consts";


const scrollToTop = () => {
    document.documentElement.scrollIntoView(true)
}

const ListItemsLogin = observer((props) => {
    const {user} = useContext(Context)

    const logOut = () => {
        user.setUserAuth({})
        user.setIsAuth(false)
        localStorage.removeItem('token')
        props.setDropdownUser(false)
    }

    const closeDropdown = () => {
        props.setDropdownUser(false)
    }

    return (
        <div className={style.itemsLogin}>
            <div className={style.items}>
                <div className={style.item}>
                    <NavLink className={style.link} to={`${ORDER_ROUTE}/${user.user.id}`}><span onClick={()=> {
                        closeDropdown()
                        scrollToTop()
                    }
                    }>Orders</span></NavLink>
                </div>
                <div className={style.item}>
                    <NavLink className={style.link} to={`${BASKET_ROUTE}/${user.user.id}`}><span onClick={()=> {
                        closeDropdown()
                        scrollToTop()
                    }
                    }>Basket</span></NavLink>
                </div>
                <div className={style.item}>
                    <NavLink className={style.link} to={`${FAVORITE_ROUTE}/${user.user.id}`}><span onClick={()=> {
                        closeDropdown()
                        scrollToTop()
                    }
                    }>Favorites</span></NavLink>
                </div>
                <div className={style.item}>
                    <NavLink className={style.link} to={CHAT_ROUTE}><span onClick={()=> {
                        closeDropdown()
                        scrollToTop()
                    }
                    }>Online-chat</span></NavLink>
                </div>
                <div className={style.logoutAndClose}>
                    <div className={style.item}><span onClick={logOut}>Logout</span></div>
                    <div className={style.item}><span onClick={() => props.setDropdownUser(false)}>Close</span></div>
                </div>
            </div>
        </div>
    );
});

export default ListItemsLogin;