import React, {useContext, useState} from 'react'
import style from './navbar.module.css'
import login from '../../images/navbar-images/account-person-profile-silhouette-user-username-icon-133467.png'
import {NavLink} from "react-router-dom";
import {Context} from "../../index";
import {
    ADMIN_ROUTE,
    BASKET_ROUTE,
    FAVORITE_ROUTE,
    LOGIN_ROUTE,
    MAIN_ROUTE,
    SHOP_ROUTE
} from "../../utils/consts";
import {observer} from "mobx-react-lite";
import basketImg from '../../images/navbar-images/basket.png'
import favCLothesImg from '../../images/shopPage-images/unLike.png'
import arrowDown from '../../images/navbar-images/listItemsSubcultres.png'
import arrowUp from '../../images/navbar-images/dropdownUp.png'
import ListItemsSubcultures from "./LIstItemsSubcultures/ListItemsSubcultures";
import ListItemsLogin from "./ListItemsLogin/ListItemsLogin";

const scrollToTop = () => {
    document.documentElement.scrollIntoView(true)
}

const NavigationBar = observer(() => {
    const {user} = useContext(Context)

    const [dropdownUser, setDropdownUser] = useState(false)
    const [dropdown, setDropdown] = useState(false)

    return (
        <div className={style.navbar}>
            <div className={style.listNavbar}>
                <div className={style.elementsListNavBar}>
                    <NavLink className={style.link} activeClassName={style.activeLink} exact
                             to={MAIN_ROUTE}><span onClick={scrollToTop}>Main</span></NavLink>
                </div>
                <div className={style.elementsListNavBar}>
                    {dropdown
                        ?
                        <div>
                            <span className={style.link} onClick={() => setDropdown(false)}>Subcultures <img
                                src={arrowUp} alt="err"/>
                             </span>
                            <ListItemsSubcultures dropdown={dropdown} setDropdown={setDropdown}/>
                        </div>

                        :
                        <span className={style.link} onClick={() => setDropdown(true)}>Subcultures <img
                            src={arrowDown} alt="err"/>
                         </span>

                    }

                </div>
                <div className={style.elementsListNavBar}>
                    <NavLink className={style.link} activeClassName={style.activeLink} to={SHOP_ROUTE}><span onClick={scrollToTop}>Store</span></NavLink>
                </div>
                <div className={style.elementsListNavBar}>
                    {user.user.role === 'ADMIN' && <NavLink className={style.link} activeClassName={style.activeLink} to={ADMIN_ROUTE}><span onClick={scrollToTop}>Admin</span></NavLink>}
                </div>
            </div>
            <div className={style.login}>
                {user.isAuth ?
                    <div className={style.userAuth}>
                        <NavLink to={`${FAVORITE_ROUTE}/${user.user.id}`}><img src={favCLothesImg} alt="error" onClick={scrollToTop}/></NavLink>
                        <NavLink to={`${BASKET_ROUTE}/${user.user.id}`}><img src={basketImg} alt="error" onClick={scrollToTop}/></NavLink>
                        {dropdownUser ?
                            <>
                                <img src={login} onClick={()=> setDropdownUser(false)} alt="error"/>
                                <ListItemsLogin setDropdownUser={setDropdownUser}/>
                            </>
                            :
                            <img src={login} onClick={()=> setDropdownUser(true)} alt="error"/>
                        }

                    </div>
                    :
                    <div className={style.userNotAuth}>
                        <NavLink to={LOGIN_ROUTE} className={style.link}>
                            <span>Sign in / Register</span>
                        </NavLink>
                    </div>
                }
            </div>

        </div>
    )
})

export default NavigationBar