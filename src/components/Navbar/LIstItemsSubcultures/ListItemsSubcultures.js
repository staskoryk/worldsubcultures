import React from 'react';
import style from './ListItemsSubcultures.module.css'
import {NavLink} from "react-router-dom";
import {GOTH_ROUTE, HIPPIE_ROUTE, PUNK_ROUTE, SKINHEAD_ROUTE} from "../../../utils/consts";

const listSubcultures = [
    {id:1, subculture: 'Punks', route: PUNK_ROUTE},
    {id:2, subculture: 'Skinheads', route: SKINHEAD_ROUTE},
]

const scrollToTop = () => {
    document.documentElement.scrollIntoView(true)
}

const ListItemsSubcultures = (props) => {
    return (
        <div className={style.listMenu}>
            <div className={style.listItem}>
                {listSubcultures.map(item => <div className={style.list}>
                    <span id={item.id} onClick={() => props.setDropdown(false) }><NavLink className={style.link} activeClassName={style.activeLink} to={item.route}><span onClick={scrollToTop}>{item.subculture}</span></NavLink></span>
                </div>)}
            </div>
        </div>
    );
};

export default ListItemsSubcultures;