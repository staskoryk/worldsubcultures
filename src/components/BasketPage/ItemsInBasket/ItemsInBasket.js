import React, {useContext, useState} from 'react';
import style from "../BasketPage.module.css";
import {observer} from "mobx-react-lite";
import {deleteInBasket} from "../../../http/userAPI";
import {Context} from "../../../index";

const ItemsInBasket = observer((props) => {

    const [deleteClothes, setDeleteClothes] = useState(false)

    const {user} = useContext(Context)

    const deleteClothesOnBasket = async () => {
        try {
            await deleteInBasket(user.user.id, props.clothes._id)
        } catch (e) {
            alert(e.response.data.message)
        }
    }

    return (
        <div className={style.basketClothesBlock} id={props.clothes.id} onMouseEnter={()=>setDeleteClothes(true)} onMouseLeave={()=> setDeleteClothes(false)}>
            <div className={style.clothesBasket}>
                <div className={style.imgClothes}>
                    <img src={props.clothes.imageSrc} alt=""/>
                </div>
                <div className={style.clothesInfo}>
                    <div className={style.headerClothes}>
                        <span>{props.clothes.name}</span>
                    </div>
                    <div className={style.descriptionClothes}>
                        <div>
                            <span>Sex: {props.clothes.sex}</span>
                        </div>
                        <div>
                            <span>Subculture: {props.clothes.type}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div className={style.clothesCount}>
                <span>{props.clothes.count}</span>
            </div>
            <div className={style.clothesPrice}>
                <span>{props.clothes.price}$</span>
                {deleteClothes &&
                <div className={style.deleteItem}>
                    <button onClick={deleteClothesOnBasket}>Delete</button>
                </div>
                }
            </div>

        </div>
    );
});

export default ItemsInBasket;