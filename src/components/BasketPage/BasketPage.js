import React, {useContext, useEffect, useState} from 'react';
import style from './BasketPage.module.css'
import {NavLink, useParams} from "react-router-dom";
import {SHOP_ROUTE} from "../../utils/consts";
import car from '../../images/basketPage-images/car.png'
import card from '../../images/basketPage-images/credit-card.png'
import HQ from '../../images/basketPage-images/high-quality.png'
import {observer} from "mobx-react-lite";
import {Context} from "../../index";
import addToBasket from '../../images/basketPage-images/add-to-cart.png'
import ItemsInBasket from "./ItemsInBasket/ItemsInBasket";
import {fetchBasket} from "../../http/userAPI";
import {addOrder} from "../../http/orderAPI";
import {useInput} from "../../utils/customHooks";
import Validation from "../../utilsComponents/Validation/Validation";
import Helper from "../../utilsComponents/helper/helper";




const BasketPage = observer(() => {
    const {user} = useContext(Context)
    const {id} = useParams()
    const [isHelper, setHelper] = useState(false)
    const [isHelperInformation, setHelperInformation] = useState('')
    const comment = useInput('', {isEmpty: true, minLength: 4, maxLength: 70})
    const phone = useInput('', {isEmpty: true, minLength: 8, maxLength: 20})
    const address = useInput('', {isEmpty: true, minLength: 10, maxLength: 30})
    const name = useInput('', {isEmpty: true, minLength: 2, maxLength: 30})


    const addNewOrder = async () => {
        try {
            await addOrder(phone.value, address.value, name.value, comment.value, user.basket.reduce((total, clothes) => {
                return total += clothes.price * clothes.count
            },0), id)
            setHelper(true)
            setHelperInformation('Placing an order successfully')
            setTimeout(() => setHelper(false), 2000)
        } catch (e) {
            setHelper(true)
            setHelperInformation(e.response.data.message)
            setTimeout(() => setHelper(false), 2000)
        }
    }

    useEffect(() => {
        fetchBasket(user.user.id).then(data => user.setBasketItems(data))
    })

    return (
        <div className={style.basketWrapper}>
            {isHelper === true && <Helper children={isHelperInformation}/>}
            {user.basket.length === 0 ?
                <div className={style.basketEmpty}>
                    <div className={style.imgBasketEmpty}>
                        <img src={addToBasket} alt=""/>
                    </div>
                    <div className={style.header}>
                        <span>Basket is empty</span>
                    </div>
                    <div className={style.text}>
                        <span>To select items, go to the catalog</span>
                    </div>
                    <div className={style.button}>
                        <NavLink to={SHOP_ROUTE}>
                            <button>Go to the catalog</button>
                        </NavLink>
                    </div>
                </div>
                :
                <div className={style.basketHaveItems}>
                    <div className={style.headerForm}>
                        <span>Order registration</span>
                    </div>
                    <div className={style.formOrder}>
                        <div className={style.formText}>
                            <div className={style.textInForm}>
                                <span>Phone:</span>
                            </div>
                            <div className={style.inputInForm}>
                                <input placeholder='Your phone' value={phone.value} onChange ={e => phone.onChange(e)} onBlur={e=> phone.onBlur(e)} type="text"/>
                                {(phone.isDirty && phone.isEmpty) &&
                                <Validation children={<div>* The field cannot be empty</div>}/>}
                                {(phone.isDirty && phone.minLengthError) &&
                                <Validation children={<div>* Invalid length</div>}/>}
                                {(phone.isDirty && phone.maxLengthError) &&
                                <Validation children={<div>* Invalid length</div>}/>}
                            </div>
                        </div>
                        <div className={style.formText}>
                            <div className={style.textInForm}>
                                <span>Address:</span>
                            </div>
                            <div className={style.inputInForm}>
                                <input placeholder='Your address' value={address.value} onChange ={e => address.onChange(e)} onBlur={e=> address.onBlur(e)} type="text"/>
                                {(address.isDirty && address.isEmpty) &&
                                <Validation children={<div>* The field cannot be empty</div>}/>}
                                {(address.isDirty && address.minLengthError) &&
                                <Validation children={<div>* Invalid length</div>}/>}
                                {(address.isDirty && address.maxLengthError) &&
                                <Validation children={<div>* Invalid length</div>}/>}
                            </div>
                        </div>
                        <div className={style.formText}>
                            <div className={style.textInForm}>
                                <span>Name:</span>
                            </div>
                            <div className={style.inputInForm}>
                                <input placeholder='Your name' value={name.value} onChange ={e => name.onChange(e)} onBlur={e=> name.onBlur(e)} type="text"/>
                                {(name.isDirty && name.isEmpty) &&
                                <Validation children={<div>* The field cannot be empty</div>}/>}
                                {(name.isDirty && name.minLengthError) &&
                                <Validation children={<div>* Invalid length</div>}/>}
                                {(name.isDirty && name.maxLengthError) &&
                                <Validation children={<div>* Invalid length</div>}/>}
                            </div>
                        </div>
                        <div className={style.formText}>
                            <div className={style.textInForm}>
                                <span>Comment:</span>
                            </div>
                            <div className={style.inputInForm}>
                                <textarea placeholder='Your comment' value={comment.value} onChange ={e => comment.onChange(e)} onBlur={e=> comment.onBlur(e)} type="text"/>
                                {(comment.isDirty && comment.isEmpty) &&
                                <Validation children={<div>* The field cannot be empty</div>}/>}
                                {(comment.isDirty && comment.minLengthError) &&
                                <Validation children={<div>* Invalid length</div>}/>}
                                {(comment.isDirty && comment.maxLengthError) &&
                                <Validation children={<div>* Invalid length</div>}/>}
                            </div>
                        </div>
                    </div>
                    <div className={style.order}>
                        <h1>Clothes worth: <span>{user.basket.reduce((total, clothes) => {
                            return total += clothes.price * clothes.count
                        },0)}</span>$</h1>
                        <div className={style.buttonOrder}>
                            <button disabled={!phone.inputValid || !address.inputValid || !name.inputValid || !comment.inputValid} onClick={addNewOrder}>Place an order</button>
                        </div>
                    </div>

                    <div className={style.cartInfo}>
                        <div className={style.cartInfoClothes}>
                            <span>{user.basket.length} clothes</span>
                        </div>
                        <div className={style.cartInfoCountClothes}>
                            <span>Count</span>
                        </div>
                        <div className={style.cartInfoPriceClothes}>
                            <span>Price</span>
                        </div>
                    </div>

                    <div className={style.allItemsOnBasket}>
                        {user.basket.map(clothes =>
                            <ItemsInBasket clothes={clothes}/>
                        )}
                    </div>
                </div>
            }

            <div className={style.informationOrder}>
                <div className={style.InfoBlockDelivery}>
                    <div className={style.imgOrder}>
                        <img src={car} alt=""/>
                    </div>
                    <div className={style.headerInInformation}>
                        <span>Free shipping</span>
                    </div>
                    <div className={style.infoText}>
                        <p>Free delivery throughout the territory of the Republic of Belarus.</p>
                    </div>
                </div>
                <div className={style.informationPayment}>
                    <div className={style.imgOrder}>
                        <img src={card} alt=""/>
                    </div>
                    <div className={style.headerInInformation}>
                        <span>Payment by card or cash</span>
                    </div>
                    <div className={style.infoText}>
                        <p>Payment for clothing is made only at a meeting with the seller.</p>
                    </div>
                </div>
                <div className={style.informationBrands}>
                    <div className={style.imgOrder}>
                        <img src={HQ} alt=""/>
                    </div>
                    <div className={style.headerInInformation}>
                        <span>Only high-quality clothing</span>
                    </div>
                    <div className={style.infoText}>
                        <p>Our store guarantees the quality and authenticity of every item you buy from us. If the item
                            does not fit you, you have 14 days to return it with a 100% refund.</p>
                    </div>
                </div>
            </div>
        </div>
    );
});

export default BasketPage;