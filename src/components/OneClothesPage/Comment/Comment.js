import React from 'react';
import style from "../OneClothesPage.module.css";
import star from "../../../images/shopPage-images/star.png";

const Comment = ({comments}) => {
    return (
        <div className={style.reviews}>
            <div className={style.userRate}>
                <img src={star} alt=""/>
                <span>{comments.rate}</span>
            </div>
            <div className={style.usernameAndUserReview}>
                <div className={style.username}>
                    <span>{comments.name}</span>
                </div>
                <div className={style.userReview}>
                    <div className={style.comment}>
                        <p>{comments.comment}</p>
                    </div>

                </div>
            </div>
        </div>
    );
};

export default Comment;