import React, {useContext, useEffect, useState} from 'react';
import style from './OneClothesPage.module.css'
import {NavLink, useParams} from 'react-router-dom'
import {addComment, deleteClothes, fetchComments, fetchOneClothes} from "../../http/clothesAPI";
import {addItemToBasket} from "../../http/userAPI";
import {observer} from "mobx-react-lite";
import {Context} from "../../index";
import star from '../../images/shopPage-images/star.png'
import Comment from "./Comment/Comment";
import {LOGIN_ROUTE, SHOP_ROUTE} from "../../utils/consts";
import {useInput} from "../../utils/customHooks";
import Validation from "../../utilsComponents/Validation/Validation";
import Helper from "../../utilsComponents/helper/helper";
import del from '../../images/shopPage-images/delete.png'



const OneClothesPage = observer(() => {
    const [clothes, setClothes] = useState({})
    const [comments, setComments] = useState([])
    const {id} = useParams()
    const name = useInput('', {isEmpty: true, minLength: 2, maxLength: 10})
    const rate = useInput('', {isEmpty: true})
    const comment = useInput('', {isEmpty: true, minLength: 10, maxLength: 100})
    const {user} = useContext(Context)
    const [isHelper, setHelper] = useState(false)
    const [isHelperInformation, setHelperInformation] = useState('')
    useEffect(() => {
        fetchOneClothes(id).then(data => setClothes(data))
    }, [])

    const deleteClot = async () => {
        try {
            await deleteClothes(id)
        } catch (e) {
            alert(e.response.data.message)
        }
    }

    useEffect(() => {
        fetchComments(id).then(data => setComments(data))
    }, [comments])

    const addComments = async () => {
        try {
            await addComment(name.value, rate.value, comment.value, clothes._id)
            name.setValueNull()
            rate.setValueNull()
            comment.setValueNull()
        } catch (e) {
            alert(e.response.data.message)
        }
    }

    const addToBasket = async () => {
        try {
            await addItemToBasket(user.user.id, id)
            setHelper(true)
            setHelperInformation('Product added')
            setTimeout(() => setHelper(false), 2000)
        } catch (e) {
            alert(e.response.data.message)
        }
    }


    return (
        <div className={style.oneClothesWrapper}>
            {isHelper === true && <Helper children={isHelperInformation}/>}
            <div className={style.oneClothes}>
                <div className={style.imgBlock}>
                    <NavLink to={SHOP_ROUTE}>{user.user.role === 'ADMIN' && <img src={del} className={style.del} onClick={deleteClot} alt=""/>}</NavLink>
                    <img src={clothes.imageSrc} alt=""/>
                </div>
                <div className={style.infoBlock}>
                    <div className={style.infoName}>
                        <span>{clothes.name}</span>
                    </div>
                    <div className={style.infoPrice}>
                        <span>Price: {clothes.price}$</span>
                    </div>
                    <div className={style.infoSex}>
                        <span>Sex: {clothes.sex}</span>
                    </div>
                    <div className={style.infoSubcultures}>
                        <span>Subculture: {clothes.type}</span>
                    </div>
                    <div className={style.infoDescription}>
                        <span>{clothes.description}</span>
                    </div>
                    <div className={style.infoButton}>
                        {user.isAuth === false ? <NavLink to={LOGIN_ROUTE}><button>Add to cart</button></NavLink> :
                        <button onClick={addToBasket}>Add to cart</button>}
                    </div>
                </div>
            </div>

            <div className={style.avgReview}>
                {comments.length === 0 && <span>No rating</span> }
                {comments.length >= 1 && <span>Average rating: {(comments.reduce((total, comment) => total + comment.rate, 0) / comments.length).toFixed(1)} </span>}
                <img src={star} alt=""/>
            </div>

            <div className={style.blockWithReview}>
                {comments.length === 0 ? <h1 style={{textAlign:'center'}}>Comments is null</h1> : comments.map(comments => <Comment comments={comments}/>)}

                <div className={style.feedback}>
                    <span>Leave your feedback about these clothes</span>
                </div>
                <div className={style.sendUserNameReview}>
                    <div className={style.feedbacksWrapper}>
                        <div className={style.feedbackSpan}>
                            <span>Your name:</span>
                        </div>
                        <div className={style.feedbackInput}>
                            <input value={name.value} onChange={e => name.onChange(e)} onBlur={e => name.onBlur(e)} type="text"/>
                            {(name.isDirty && name.isEmpty) &&
                            <Validation children={<div>* The field cannot be empty</div>}/>}
                            {(name.isDirty && name.minLengthError) &&
                            <Validation children={<div>* Invalid length</div>}/>}
                            {(name.isDirty && name.maxLengthError) &&
                            <Validation children={<div>* Invalid length</div>}/>}
                        </div>
                    </div>
                    <div className={style.feedbacksWrapper}>
                        <div className={style.feedbackSpan}>
                            <span>Rate it:</span>
                        </div>
                        <div className={style.feedbackInput}>
                            <select value={rate.value} name="rate" onChange={e => rate.onChange(e)} onBlur={e => rate.onBlur(e)}>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div>
                    </div>
                    <div className={style.feedbacksWrapper}>
                        <div className={style.feedbackSpan}>
                            <span>Your comment:</span>
                        </div>
                        <div className={style.feedbackInput}>
                            <textarea value={comment.value} onChange={e => comment.onChange(e)} onBlur={e => comment.onBlur(e)}>

                            </textarea>
                            {(comment.isDirty && comment.isEmpty) &&
                            <Validation children={<div>* The field cannot be empty</div>}/>}
                            {(comment.isDirty && comment.minLengthError) &&
                            <Validation children={<div>* Invalid length</div>}/>}
                            {(comment.isDirty && comment.maxLengthError) &&
                            <Validation children={<div>* Invalid length</div>}/>}
                        </div>
                    </div>
                    <div className={style.feedbackBtn}>
                        <button disabled={!name.inputValid || !rate.inputValid || !comment.inputValid} onClick={addComments}>Send comment</button>
                    </div>
                </div>
            </div>
        </div>
    );
});

export default OneClothesPage;