import React from 'react';
import style from "./Orders.module.css";

const Orders = ({order}) => {
    return (
        <>
            <div className={order.isReview === true ? style.orderTrue : style.orderFalse}>
                <div className={style.elementOrder}>
                    <span>Name: {order.name}</span>
                </div>
                <div className={style.elementOrder}>
                    <span>Phone: {order.phone}</span>
                </div>
                <div className={style.elementOrder}>
                    <span>Address: {order.address}</span>
                </div>
                <div className={style.elementOrder}>
                    <span>Price: {order.price}$</span>
                </div>
                <div className={style.elementOrder}>
                    <span>Date: {order.date}</span>
                </div>
                <div className={style.elementClothes}>
                    <span>Clothes:</span>
                    {order.clothes.map(clothes =>
                        <div className={style.clothes}>
                            <span>{clothes.clothes.name} ({clothes.count})</span>
                        </div>
                    )}
                </div>
            </div>
        </>

    );
};

export default Orders;