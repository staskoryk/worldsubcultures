import React, {useContext, useEffect} from 'react';
import style from './OrderPage.module.css'
import {observer} from "mobx-react-lite";
import Orders from "./Orders/Orders";
import {NavLink} from "react-router-dom"
import {Context} from "../../index";
import {fetchOneUserOrders} from "../../http/orderAPI";
import {BASKET_ROUTE} from "../../utils/consts";

const OrderPage = observer(() => {

    const {orders, user} = useContext(Context)


    useEffect(() => {
        fetchOneUserOrders(user.user.id).then(data => orders.setOrder(data))
    })

    return (
        <div className={style.orderWrapper}>
            <div className={style.orderHeader}>
                {orders.order.length === 0 ?
                    <div className={style.ordersIsEmpty}>
                        <h1>Your orders is empty</h1>
                        <div className={style.spantext}><span>Place an order in the shopping cart</span></div>
                        <NavLink to={`${BASKET_ROUTE}/${user.user.id}`}><button>Go to basket</button></NavLink>
                    </div> :
                    <h1>Your orders</h1>
                }
            </div>
            <div className={style.orders}>
                <>
                    {orders.order.map(order =>
                        <Orders order={order}/>)}
                </>
            </div>
        </div>
    );
});


export default OrderPage;