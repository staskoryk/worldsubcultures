import React, {useContext, useEffect, useState} from 'react';
import style from './ChatPage.module.css'
import {observer} from "mobx-react-lite";
import {Context} from "../../index";
import {deleteAllMessages, getMessages, sendMessage} from "../../http/userAPI";
import Message from "./message";



const ChatPage = observer(() => {

    const {user} = useContext(Context)

    const [message, setMessage] = useState('')
    const [messages, setMessages] = useState([])

    useEffect(() => {
        getMessages().then(data => setMessages(data))
    })

    const send = async () => {
        try {
            await sendMessage(user.user.id, user.user.username, message)
            setMessage('')
        } catch (e) {
            alert(e.response.data.message)
        }

    }




    const clear = async () => {
        try {
            await deleteAllMessages()
        } catch (e) {
            alert(e.response.data.message)
        }

    }

    return (
        <div className={style.chatWrapper}>
            <div className={style.chat}>
                <div className={style.chatMessages}>
                    {messages.length === 0
                        ?
                        <div className={style.chatMessage}>
                            <div className={style.userNameImMessage}>
                                <span>The chat was cleared by the administrator!!!</span>
                            </div>
                        </div>
                        :
                        messages.map(message =>
                            <Message message = {message}/>
                        )
                    }
                </div>
                <div>
                    {messages.length > 0 && user.user.role === 'ADMIN' && <button className={style.clearAll} onClick={clear}>Clear chat</button>}
                </div>
                <div className={style.sendMessageBlock}>
                    <div className={style.sendMessage}>
                        <div className={style.username}>
                            <span>{user.user.username}:</span>
                        </div>
                        <div className={style.inputSendMessage}>
                            <textarea placeholder="Input your message" value={message} onChange={e => setMessage(e.target.value)}>

                            </textarea>
                        </div>
                        <div className={style.sendMessageButton}>
                            <button onClick={send}>Send</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
});

export default ChatPage;