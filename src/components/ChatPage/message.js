import React, {useContext} from 'react';
import style from "./ChatPage.module.css";
import {deleteOneMessage} from "../../http/userAPI";
import {observer} from "mobx-react-lite";
import {Context} from "../../index";


const Message = observer(({message}) => {

    const {user} = useContext(Context)

    const deleteMessage = async (messageId) => {
        try {
            await deleteOneMessage(message._id)
        } catch (e) {
            alert(e.response.data.message)
        }

    }
    return (
        <div className={style.chatMessage}>
            <div className={style.userNameImMessage}>
                <span>{message.name}: {user.user.role === 'ADMIN' && <button onClick={deleteMessage}>X</button>}
                    <p className={style.userMessage}>{message.message}</p></span>
            </div>
        </div>
    );
});

export default Message;