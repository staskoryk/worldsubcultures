import React from 'react';
import style from './Footer.module.css'
import fb from '../../images/footer-images/facebook.png'
import inst from '../../images/footer-images/inst.png'
import vk from '../../images/footer-images/vk.png'
import git from '../../images/footer-images/git.png'
import {NavLink} from "react-router-dom";
import {ABOUT_ROUTE} from "../../utils/consts";

const scrollToTop = () => {
    document.documentElement.scrollIntoView(true)
}

const Footer = () => {
    return (
        <div className={style.footer}>
            <div className={style.PartOne}>
                <span>
                    <a href="https://www.facebook.com/profile.php?id=100007059885696" target='_blank' rel='noreferrer'><img src={fb} alt=""/></a>
                   <a href="https://www.instagram.com/staskorik/" target='_blank' rel='noreferrer'><img src={inst} alt=""/></a>
                   <a href="https://vk.com/st.koryk" target='_blank' rel='noreferrer'><img src={vk} alt=""/></a>
                   <a href="https://gitlab.com/staskoryk" target='_blank' rel='noreferrer'><img src={git} alt=""/></a>
                </span>
            </div>
            <div className={style.PartTwo}>
                <span onClick={scrollToTop}><NavLink className={style.link} to={ABOUT_ROUTE}>About</NavLink></span>
            </div>
            <div className={style.PartThree}>
                <span> &#169; 2021 St.Koryk Inc. All rights reserved</span>
            </div>
        </div>
    );
};

export default Footer;