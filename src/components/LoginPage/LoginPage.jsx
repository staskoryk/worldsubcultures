import React, {useContext, useState} from 'react';
import {observer} from "mobx-react-lite";
import {Context} from "../../index";
import {login} from "../../http/userAPI";
import style from "./LoginPage.module.css";
import {NavLink, Redirect} from "react-router-dom";
import {MAIN_ROUTE, REGISTRATION_ROUTE} from "../../utils/consts";
import bgImage from "../../images/punkPage-images/PartSixFullImg.png"
import Validation from "../../utilsComponents/Validation/Validation";
import {useInput} from "../../utils/customHooks";
import Helper from "../../utilsComponents/helper/helper";



const LoginPage = observer(() => {
    const {user} = useContext(Context)
    const userLogin = useInput('', {isEmpty: true, minLength: 4, maxLength: 20})
    const password = useInput('', {isEmpty: true, minLength: 4, maxLength: 20})

    const [isHelper, setHelper] = useState(false)
    const [isHelperInformation, setHelperInformation] = useState('')

    const auth = async () => {
        try {
            let data
            data = await login(password.value, userLogin.value)
            user.setUserAuth(data)
            user.setIsAuth(true)
        } catch (e) {
            setHelper(true)
            setHelperInformation(e.response.data.message)
            setTimeout(() => setHelper(false), 2000)
        }

    }
    return (
        <div className={style.wrapper} style={{backgroundImage: `url(${bgImage})`}}>
            {isHelper === true && <Helper children={isHelperInformation}/>}
            <div className={style.loginWrapper}>
                <div className={style.header}>
                    <h1>You are welcome!</h1>
                </div>
                <div className={style.text}>
                    <h2>Before you start, fill in the details</h2>
                </div>
                <div className={style.form}>
                    <div>
                        <div className={style.inputWithText}>
                            <h2 className={style.textInput}>
                                Your login:
                            </h2>
                            <input type="text" placeholder="Enter your login" value={userLogin.value}
                                   onChange={e => userLogin.onChange(e)} onBlur={e => userLogin.onBlur(e)}/>
                            {(userLogin.isDirty && userLogin.isEmpty) &&
                            <Validation children={<div>* The field cannot be empty</div>}/>}
                            {(userLogin.isDirty && userLogin.minLengthError) &&
                            <Validation children={<div>* Invalid length</div>}/>}
                            {(userLogin.isDirty && userLogin.maxLengthError) &&
                            <Validation children={<div>* Invalid length</div>}/>}
                        </div>
                        <div className={style.inputWithText}>
                            <h2 className={style.textInput}>
                                Your password:
                            </h2>
                            <input type="password" placeholder="Enter your password" value={password.value}
                                   onChange={e => password.onChange(e)} onBlur={e => password.onBlur(e)}/>
                            {password.isDirty && password.isEmpty &&
                            <Validation children={<div>* The field cannot be empty</div>}/>}
                            {password.isDirty && password.minLengthError &&
                            <Validation children={<div>* Invalid length</div>}/>}
                            {password.isDirty && password.maxLengthError &&
                            <Validation children={<div>* Invalid length</div>}/>}
                        </div>
                        <div className={style.registration}>
                            <h4>No account? <NavLink className={style.navLink} to={REGISTRATION_ROUTE}>
                                <span>Registration</span>
                            </NavLink>
                            </h4>
                        </div>
                        <div className={style.button}>
                            <button disabled={!userLogin.inputValid || !password.inputValid} onClick={auth}>Enter</button>
                            {user.isAuth && <Redirect to={MAIN_ROUTE}/>}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    );
});

export default LoginPage;