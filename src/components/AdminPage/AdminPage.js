import React from 'react';
import style from './AdminPage.module.css'
import {observer} from "mobx-react-lite";
import AddClothes from "./AddClothes/AddClothes";
import DeleteClothes from "./DeleteClothes/DeleteClothes";
import EditClothes from "./EditClothes/EditClothes";
import AdminOrders from "./AdminOrders/AdminOrders";

const AdminPage = observer(() => {

    return (
        <>
            <div className={style.adminWrapper}>
                <AddClothes/>
                <DeleteClothes/>
                <EditClothes/>
            </div>
           <AdminOrders/>
        </>

    )
});

export default AdminPage;