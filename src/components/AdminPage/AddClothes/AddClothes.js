import React, {useState} from 'react';
import style from "./AddClothes.module.css";
import {createClothes} from "../../../http/clothesAPI";
import {observer} from "mobx-react-lite";

const AddClothes = observer(() => {
    const [name, setName] = useState('')
    const [price, setPrice] = useState('')
    const [sex, setSex] = useState('')
    const [imageSrc, setImageSrc] = useState('')
    const [description, setDescription] = useState('')
    const [type, setType] = useState('')

    const addClothes = async() => {
        try {
            await createClothes(name, price, sex, imageSrc, description, type)
        } catch (e) {
            alert(e.response.data.message)
        }
    }

    return (
        <div className={style.formAdmin}>
            <h1>Add clothes</h1>
            <div className={style.elementsInForm}>
                <div className={style.elementInForm}>
                    <div className={style.spanInForm}><span>Name:</span></div>
                    <div className={style.inputInForm}><input type="text" value={name} onChange={e => setName(e.target.value)}/></div>
                </div>
                <div className={style.elementInForm}>
                    <div className={style.spanInForm}><span>Price:</span></div>
                    <div className={style.inputInForm}><input value={price} onChange={e => setPrice(e.target.value)} type="number"/></div>
                </div>
                <div className={style.elementInForm}>
                    <div className={style.spanInForm}><span>Sex:</span></div>
                    <div className={style.inputInForm}>
                        <select name="sex" value={sex} onChange={e => setSex(e.target.value)}>
                            <option style={{color: 'grey'}}>Choose sex...</option>
                            <option>Men</option>
                            <option>Women</option>
                        </select>
                    </div>
                </div>
                <div className={style.elementInForm}>
                    <div className={style.spanInForm}><span>ImageSrc:</span></div>
                    <div className={style.inputInForm}><input type="text" value={imageSrc} onChange={e => setImageSrc(e.target.value)}/></div>
                </div>
                <div className={style.elementInForm}>
                    <div className={style.spanInForm}><span>Description:</span></div>
                    <div className={style.inputInForm}><textarea value={description} onChange={e => setDescription(e.target.value)}>
                        </textarea></div>
                </div>
                <div className={style.elementInForm}>
                    <div className={style.spanInForm}><span>Type of subcultures:</span></div>
                    <div className={style.inputInForm}>
                        <select name="type" value={type} onChange={e => setType(e.target.value)}>
                            <option style={{color: 'grey'}}>Choose type...</option>
                            <option>Punks</option>
                            <option>Skinheads</option>
                            <option>Hippies</option>
                            <option>Goths</option>
                        </select>
                    </div>
                </div>
            </div>
            <div className={style.buttonInForm}>
                <button onClick={addClothes}>Add clothes</button>
            </div>
        </div>
    );
});

export default AddClothes;