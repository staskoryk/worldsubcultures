import React from 'react';
import style from "./AdminOrder.module.css";
import {deleteOrder} from "../../../../http/orderAPI";

const AdminOrderTrue = ({order}) => {

    const deleteOrders = async () => {
        try {
            await deleteOrder(order._id)
        } catch (e) {
            alert(e.response.data.message)
        }
    }

    return (
        <>
            {order.isReview === true &&
            <div className={style.orderTrue}>
                <div className={style.elementOrder}>
                    <span>Name: {order.name}</span>
                </div>
                <div className={style.elementOrder}>
                    <span>Phone: {order.phone}</span>
                </div>
                <div className={style.elementOrder}>
                    <span>Address: {order.address}</span>
                </div>
                <div className={style.elementOrder}>
                    <span>Price: {order.price}$</span>
                </div>
                <div className={style.elementOrder}>
                    <span>Date: {order.date}</span>
                </div>
                <div className={style.elementOrder}>
                    <span>UserId: {order.user.userId}</span>
                </div>
                <div className={style.elementOrder}>
                    <span>Clothes: </span>
                    <div className={style.clothes}>
                        {order.clothes.map(clothes =>
                            <div><span>{clothes.clothes.name} ({clothes.count})</span></div>
                        )}
                    </div>
                </div>
                <div className={style.elementOrder}>
                    <button onClick={deleteOrders}>Delete</button>
                </div>
            </div>
            }

        </>

    );
};

export default AdminOrderTrue;