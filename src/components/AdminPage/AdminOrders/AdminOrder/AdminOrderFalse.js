import React from 'react';
import style from "./AdminOrder.module.css";
import {deleteOrder, reviewOrder} from "../../../../http/orderAPI";
import {observer} from "mobx-react-lite";

const AdminOrderFalse = observer(({order}) => {


    const deleteOrders = async () => {
        try {
            await deleteOrder(order._id)
        } catch (e) {
            alert(e.response.data.message)
        }
    }

    const reviewOrders = async () => {
        try {
            await reviewOrder(order._id)
        } catch (e) {
            alert(e.response.data.message)
        }
    }

    return (
        <>
            {order.isReview === false &&
            <div className={style.orderFalse}>
                <div className={style.elementOrder}>
                    <span>Name: {order.name}</span>
                </div>
                <div className={style.elementOrder}>
                    <span>Phone: {order.phone}</span>
                </div>
                <div className={style.elementOrder}>
                    <span>Address: {order.address}</span>
                </div>
                <div className={style.elementOrder}>
                    <span>Price: {order.price}$</span>
                </div>
                <div className={style.elementOrder}>
                    <span>Comment: {order.comment}</span>
                </div>
                <div className={style.elementOrder}>
                    <span>Date: {order.date}</span>
                </div>
                <div className={style.elementOrder}>
                    <span>UserId: {order.user.userId}</span>
                </div>
                <div className={style.elementOrder}>
                    <span>Clothes: </span>
                    <div className={style.clothes}>
                        {order.clothes.map(clothes =>
                            <div>
                                <span>{clothes.clothes.name} ({clothes.count})</span>
                            </div>
                        )}
                    </div>
                </div>
                <div className={style.elementOrder}>
                    <button onClick={deleteOrders}>Delete</button>
                </div>
                <div className={style.elementOrder}>
                    <button onClick={reviewOrders}>Place an order</button>
                </div>
            </div>
            }

        </>

    );
});

export default AdminOrderFalse;