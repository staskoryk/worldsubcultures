import React, {useContext, useEffect} from 'react';
import style from './AdminOrders.module.css'
import {Context} from "../../../index";
import {fetchAllOrders} from "../../../http/orderAPI";
import {observer} from "mobx-react-lite";
import AdminOrderFalse from "./AdminOrder/AdminOrderFalse";
import AdminOrderTrue from "./AdminOrder/AdminOrderTrue";

const AdminOrders = observer(() => {

    const {orders} = useContext(Context)

    useEffect(() => {
        fetchAllOrders().then(data => orders.setOrder(data))
    })

    return (
        <div className={style.wrapperOrders}>
            <h1>Orders not placed</h1>
            <div className={style.orders}>
                {orders.order.map(order =>
                    <AdminOrderFalse order={order}/>
                )}
            </div>
            <h1>Orders placed</h1>
            <div className={style.orders}>
                {orders.order.map(order => <AdminOrderTrue order={order}/>)}
            </div>
        </div>
    );
});

export default AdminOrders;