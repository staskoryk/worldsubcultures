import React, {useState} from 'react';
import style from "./DeleteClothes.module.css";
import {observer} from "mobx-react-lite";
import {deleteClothes} from "../../../http/clothesAPI";

const DeleteClothes = observer(() => {



    const [clothesId, setClothesId] = useState('')
    const deleteClot = async () => {
        try {
            await deleteClothes(clothesId)
        } catch (e) {
            alert(e.response.data.message)
        }
    }
    return (
        <div className={style.deleteForm}>
            <h1>Delete clothes</h1>
            <div className={style.span}> <span>Id clothes:</span> </div>
            <div className={style.input}><input type="text" value={clothesId} onChange={e=> setClothesId(e.target.value)}/></div>
            <div className={style.button}><button onClick={deleteClot}>Delete clothes</button></div>
        </div>
    );
});

export default DeleteClothes;