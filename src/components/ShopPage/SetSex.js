import React, {useContext, useState} from 'react';
import style from "./ShopPage.module.css";
import {Context} from "../../index";

const SetSex = ({sex}) => {

    const [active, setActive] = useState(false)
    const {clothes} = useContext(Context)

    return (
        <span onClick={() => setActive(true)}>
            <button className={active === false ? style.button : style.buttonActive}
                    onClick={() => clothes.setSex(sex.setSex)}>{sex.name}</button>
        </span>
    );
};

export default SetSex;