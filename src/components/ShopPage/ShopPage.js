import React, {useContext, useEffect, useState} from 'react';
import style from './ShopPage.module.css'
import {observer} from "mobx-react-lite";
import {Context} from "../../index";
import ListClothesItem from "./ListClothesItem/ListClothesItem";
import {fetchAllClothes} from "../../http/clothesAPI";
import clear from "../../images/shopPage-images/clear.png"
import search from '../../images/shopPage-images/search.png'

const ShopPage = observer(() => {

    const {clothes, user} = useContext(Context)


    useEffect(() => {
        fetchAllClothes(clothes.type, clothes.sex, clothes.name).then(data => clothes.setClothes(data))
    }, [clothes.type, clothes.sex, clothes.name])

    const [sexMan, setSexMan] = useState(false)
    const [sexWomen, setSexWomen] = useState(false)

    return (
        <div className={style.shopWrapper}>
            <div className={style.sortAndFindItems}>
                <div className={style.sort}>
                    {sexMan === false ? <button className={style.button} onClick={() => {
                            clothes.setSex('Men')
                            setSexMan(true)
                            setSexWomen(false)
                        }}>MEN</button>
                        :
                        <button className={style.buttonActive} onClick={() => {
                            clothes.setSex('Men')
                        }}>MEN</button>
                    }
                    {sexWomen === false ?
                        <button className={style.button} onClick={() => {
                            clothes.setSex('Women')
                            setSexWomen(true)
                            setSexMan(false)
                        }}>WOMEN</button>
                        :
                        <button className={style.buttonActive} onClick={() => {
                            clothes.setSex('Women')
                        }
                        }>WOMEN</button>
                    }
                    <img src={clear} onClick={() => {
                        clothes.setSex('')
                        setSexWomen(false)
                        setSexMan(false)
                    }
                    } alt=""/>

                    <div className={style.sortSubcultures}>
                        <span>Subcultures:</span>
                        <button className={clothes.type === 'Punks' ? style.buttonActive : style.button} onClick={() => clothes.setType('Punks')}>Punks</button>
                        <button className={clothes.type === 'Skinheads' ? style.buttonActive : style.button} onClick={() => clothes.setType('Skinheads')}>Skinheads</button>
                        <button className={clothes.type === 'Hippies' ? style.buttonActive : style.button} onClick={() => clothes.setType('Hippies')}>Hippies</button>
                        <button className={clothes.type === 'Goths' ? style.buttonActive : style.button} onClick={() => clothes.setType('Goths')}>Goths</button>
                        <img src={clear} onClick={()=> clothes.setType('')} alt=""/>
                    </div>
                </div>
                <div className={style.find}>
                    <span className={style.search}>
                        <img src={search} alt=""/>
                    </span>
                    <input placeholder='Search' value={clothes.name} onChange={e => clothes.setName(e.target.value)} type="text"/>
                    <img src={clear} alt="delete" onClick={()=>clothes.setName('')}/>
                </div>

            </div>

            <div className={style.items}>
                {   clothes.clothes.length === 0 ? <div className={style.clothesIsNull}>Products not found
                </div>
                    :
                    clothes.clothes.map(clothes =>
                    <ListClothesItem id={clothes._id} clothes={clothes} user={user}/>
                )}
            </div>

        </div>
    );
});

export default ShopPage;