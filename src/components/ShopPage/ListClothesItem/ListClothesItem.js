import React, {useState} from 'react';
import {useHistory} from 'react-router-dom'
import style from "./ListClothesItem.module.css";
import {CLOTHES_ROUTE} from "../../../utils/consts";
import unlike from '../../../images/shopPage-images/unLike.png'
import like from  '../../../images/shopPage-images/like.png'
import {observer} from "mobx-react-lite";
import {addItemToFavorite, deleteInFavorite} from "../../../http/userAPI";

const scrollToTop = () => {
    document.documentElement.scrollIntoView(true)
}

const ListClothesItem = observer(({clothes,user}) => {
    const history = useHistory()
    const [likeClothes, setLikeClothes] = useState(false)



    const addFavoriteClothes = async() => {
        try {
            await addItemToFavorite(user.user.id, clothes._id)
        } catch (e) {
            alert(e.response.data.message)
        }
    }

    const deleteFavoriteClothes = async() => {
        try {
            await deleteInFavorite(user.user.id, clothes._id)
        } catch (e) {
            alert(e.response.data.message)
        }
    }


    return (
        <div className={style.item}>
            <div className={style.imgBlock}>
                <img
                    src={clothes.imageSrc}
                    alt="error"
                    onClick={() => {
                        history.push(CLOTHES_ROUTE + '/' + clothes._id)
                        scrollToTop()
                    }}
                />
                {user.isAuth ===true && <div className={style.unLike}>
                    {likeClothes? <div onClick={deleteFavoriteClothes}><img src={like} alt="" onClick={()=> setLikeClothes(false)}/></div> :
                        <div onClick={addFavoriteClothes}><img src={unlike} alt="" onClick={()=> setLikeClothes(true)}/></div>
                    }
                </div>}
            </div>
            <div className={style.nameBlock}>
                <span>{clothes.name}</span>
            </div>
            <div className={style.priceBlock}>
                <span>{clothes.price}$</span>
            </div>
            <div className={style.buttonBlock}>
                <button onClick={() => {
                    history.push(CLOTHES_ROUTE + '/' + clothes._id)
                    scrollToTop()
                }
                }>Buy</button>
            </div>
        </div>
    );
});

export default ListClothesItem;