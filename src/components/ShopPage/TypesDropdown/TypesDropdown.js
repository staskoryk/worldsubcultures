import React from 'react';
import style from './TypesDropdown.module.css'

const types = [
    {id: 1, name:'Boots'},
    {id: 2, name:'Jeans'},
    {id: 3, name:'Hats'},
    {id: 4, name:'Accessories'},
    {id: 5, name:'Coats'},
]

const TypesDropdown = (props) => {
    return (
        <div className={style.dropdownTypes}>
            {types.map(item=> <div className={style.items} id={item.id}>
                <input type="checkbox"/> <span>{item.name}</span>
            </div>)}
            <div className={style.applyAndClear}>
                <button onClick={()=> props.setDropDown(false)}>Apply</button>
                <button>Clear</button>
            </div>
        </div>
    );
};

export default TypesDropdown;