import {$authHost} from "./index";


export const addOrder = async (phone, address, name, comment, price, userId) => {
    const response = await $authHost.post(`api/orders/createOrder/${userId}`, {phone, address, name, comment, price})
    return response
}

export const fetchOneUserOrders = async (userId) => {
    const {data} = await $authHost.get('api/orders/getOneOrder/' + userId)
    return data
}

export const fetchAllOrders = async () => {
    const {data} = await $authHost.get('api/orders/getAllOrders')
    return data
}

export const deleteOrder = async (orderId) => {
    const response = await $authHost.post(`api/orders/deleteOrder`, {orderId})
    return response
}

export const reviewOrder = async (orderId) => {
    const response = await $authHost.post(`api/orders/orderIsReview`, {orderId})
    return response
}

