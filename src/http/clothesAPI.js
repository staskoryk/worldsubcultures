import {$authHost} from "./index";


export const createClothes = async (name, price, sex, imageSrc, description, type) => {
    const response = await $authHost.post('api/clothes', {name, price, sex, imageSrc, description, type})
    return response
}

export const deleteClothes = async (clothesId) => {
    const response = await $authHost.post('api/clothes/delete', {clothesId})
    return response
}

export const fetchAllClothes = async (type, sex, name) => {
    const {data} = await $authHost.get('api/clothes', {params: {
        type, sex, name
        }})
    return data
}

export const fetchOneClothes = async (id) => {
    const {data} = await $authHost.get('api/clothes/' + id)
    return data
}

export const updateClothes = async (id, name, price, sex, imageSrc, description, type) => {
    const response = await $authHost.post('api/clothes/update', {id, name, price, sex, imageSrc, description, type})
    return response
}

export const addComment = async (nameUser, rate, comment, clothesId) => {
    const response = await $authHost.post('api/clothes/addComment', {nameUser, rate, comment, clothesId})
    return response
}

export const fetchComments = async (clothesId) => {
    const {data} = await $authHost.get('api/clothes/comments/' + clothesId)
    return data
}
