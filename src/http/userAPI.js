import {$authHost, $host} from "./index";
import jwtDecode from "jwt-decode";


export const registration = async (email, password, username) => {
    const response = await $host.post('api/user/registration', {email, password, username})
    return response
}

export const login = async (password, username) => {
    const {data} = await $host.post('api/user/login', {password, username})
    localStorage.setItem('token', data.token)
    return jwtDecode(data.token)

}

export const check = async () => {
    const {data} = await $authHost.get('api/user/auth')
    localStorage.setItem('token', data.token)
    return jwtDecode(data.token)
}

export const fetchBasket = async (userId) => {
    const {data} = await $authHost.get('api/user/getItemsOnBasket/' + userId)
    return data
}

export const addItemToBasket = async (userId, clothesId) => {
    const response = await $authHost.post('/api/user/addToBasket', {userId, clothesId})
    return response
}

export const deleteInBasket = async (userId, clothesId) => {
    const response = await $authHost.post('/api/user/deleteItemsOnBasket', {userId, clothesId})
    return response
}

export const fetchFavorites = async (userId) => {
    const {data} = await $authHost.get('api/user/getItemsOnFavorite/' + userId)
    return data
}

export const addItemToFavorite = async (userId, clothesId) => {
    const response = await $authHost.post('/api/user/addToFavorite', {userId, clothesId})
    return response
}

export const deleteInFavorite = async (userId, clothesId) => {
    const response = await $authHost.post('/api/user/deleteItemsOnFavorite', {userId, clothesId})
    return response
}

export const sendMessage = async (userId, name, message) => {
    const response = await $authHost.post('/api/user/sendMessage', {userId, name, message})
    return response
}

export const getMessages = async () => {
    const {data} = await $authHost.get('api/user/getAllMessages')
    return data
}

export const deleteAllMessages = async () => {
    const response = await $authHost.post('api/user/deleteAllMessages')
    return response
}

export const deleteOneMessage = async (messageId) => {
    const response = await $authHost.post('api/user/deleteOneMessage', {messageId})
    return response
}
