import {
    ABOUT_ROUTE,
    ADMIN_ROUTE,
    BASKET_ROUTE, CHAT_ROUTE,
    CLOTHES_ROUTE, FAVORITE_ROUTE, GOTH_ROUTE, HIPPIE_ROUTE,
    LOGIN_ROUTE,
    MAIN_ROUTE, ORDER_ROUTE,
    PUNK_ROUTE, REGISTRATION_ROUTE,
    SHOP_ROUTE, SKINHEAD_ROUTE
} from "./utils/consts";
import AdminPage from "./components/AdminPage/AdminPage";
import BasketPage from "./components/BasketPage/BasketPage";
import MainPage from "./components/MainPage/mainPage";
import PunkSubculturePage from "./components/PunkSubculturePage/PunkSubculturePage";
import ShopPage from "./components/ShopPage/ShopPage";
import OneClothesPage from "./components/OneClothesPage/OneClothesPage";
import LoginPage from "./components/LoginPage/LoginPage";
import RegistrationPage from "./components/RegistrationPage/RegistrationPage";
import HippieSubculturePage from "./components/HippieSubculturePage/HippieSubculturePage";
import SkinheadSubculturePage from "./components/SkinheadSubculturePage/SkinheadSubculturePage";
import GothsSubculturePage from "./components/GothsSubculturePage/GothsSubculturePage";
import OrderPage from "./components/OrderPage/OrderPage";
import FavoriteClothesPage from "./components/FavoriteClothesPage/FavoriteClothesPage";
import ChatPage from "./components/ChatPage/ChatPage";
import React from "react";
import AboutPage from "./components/AboutPage/AboutPage";

// const MainPage = React.lazy(() => import("./components/MainPage/mainPage"));
// const AdminPage = React.lazy(() => import("./components/AdminPage/AdminPage"));
// const ShopPage = React.lazy(() => import("./components/ShopPage/ShopPage"));
// const BasketPage = React.lazy(() => import("./components/BasketPage/BasketPage"));
// const PunkSubculturePage = React.lazy(() => import("./components/PunkSubculturePage/PunkSubculturePage"));
// const OneClothesPage = React.lazy(() => import("./components/OneClothesPage/OneClothesPage"));
// const LoginPage = React.lazy(() => import("./components/LoginPage/LoginPage"));
// const HippieSubculturePage = React.lazy(() => import("./components/HippieSubculturePage/HippieSubculturePage"));
// const SkinheadSubculturePage = React.lazy(() => import("./components/SkinheadSubculturePage/SkinheadSubculturePage"));
// const GothsSubculturePage = React.lazy(() => import("./components/GothsSubculturePage/GothsSubculturePage"));
// const OrderPage = React.lazy(() => import("./components/OrderPage/OrderPage"));
// const RegistrationPage = React.lazy(() => import("./components/RegistrationPage/RegistrationPage"));
// const FavoriteClothesPage = React.lazy(() => import("./components/FavoriteClothesPage/FavoriteClothesPage"));
// const ChatPage = React.lazy(() => import("./components/ChatPage/ChatPage"));


export const authRoutes = [
    {
        path: ADMIN_ROUTE,
        Component: AdminPage
    },
    {
        path: CHAT_ROUTE,
        Component: ChatPage
    },
    {
        path: BASKET_ROUTE + '/:id',
        Component: BasketPage
    },
    {
        path: ORDER_ROUTE + '/:id',
        Component: OrderPage
    },
    {
        path: FAVORITE_ROUTE + '/:id',
        Component: FavoriteClothesPage
    },
]

export const publicRoutes = [
    {
        path: MAIN_ROUTE,
        Component: MainPage
    },
    {
        path: LOGIN_ROUTE,
        Component: LoginPage
    },
    {
        path: REGISTRATION_ROUTE,
        Component: RegistrationPage
    },

    {
        path: PUNK_ROUTE,
        Component: PunkSubculturePage
    },
    {
        path: HIPPIE_ROUTE,
        Component: HippieSubculturePage
    },
    {
        path: SKINHEAD_ROUTE,
        Component: SkinheadSubculturePage
    },
    {
        path: GOTH_ROUTE,
        Component: GothsSubculturePage
    },
    {
        path: SHOP_ROUTE,
        Component: ShopPage
    },
    {
        path: CLOTHES_ROUTE + '/:id',
        Component: OneClothesPage
    },

    {
        path: ABOUT_ROUTE,
        Component: AboutPage
    }


]