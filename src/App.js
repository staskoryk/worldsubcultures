import React, {useContext, useEffect, useState} from 'react'
import './App.css'
import NavigationBar from "./components/Navbar/navbar";
import {BrowserRouter} from "react-router-dom";
import AppRouter from "./components/AppRouter/AppRouter";
import {observer} from "mobx-react-lite";
import {Context} from "./index";
import {check} from "./http/userAPI";
import Spinner from "./utilsComponents/Spinner/Spinner";
import Footer from "./components/Footer/Footer";



const App = observer(() => {
    const {user} = useContext(Context)
    const [loading, setLoading] = useState(true)
    useEffect(() => {
        check().then(data => {
            user.setIsAuth(true)
            user.setUserAuth(data)
        }).finally(() => setLoading(false))
    }, [])

    if (loading) {
        return <Spinner/>
    }

    return (
        <div className="app-wrapper">
            <BrowserRouter>
                <NavigationBar/>
                <AppRouter/>
                <Footer/>
            </BrowserRouter>
        </div>

    )
})

export default App
