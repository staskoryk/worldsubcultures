import React, {useState} from 'react';
import ItemList from "./itemsList/ItemList";
import style from './Li.module.css'

const Li = ({children, description}) => {
    const [isItem, setItem] = useState(false)
    return (
        <li className={style.list} onMouseEnter={()=>setItem(true)} onMouseLeave={() => setItem(false)}>{children}
            {isItem === true && <ItemList children={description}/>}
        </li>
    );
};

export default Li;