import React from 'react';
import style from './helper.module.css'

const Helper = ({children}) => {
    return (
        <div className={style.helper}>
            {children}
        </div>
    );
};

export default Helper;