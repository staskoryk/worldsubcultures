import React, {useContext} from 'react';
import style from './modalRegistration.module.css'
import {Context} from "../../index";
import {observer} from "mobx-react-lite";


const ModalRegistration = observer(
    ({children}) => {
    const {modal} = useContext(Context)
    return (
        <div className={modal.isRegistrationModal ? `${style.modal} ${style.active}` : `${style.modal}`} onClick={() => modal.setIsModalRegistration(false)}>
            <div className={modal.isRegistrationModal ? `${style.modalContent} ${style.active}` : `${style.modalContent}`} onClick={e => e.stopPropagation()}>
                {children}
            </div>
        </div>
    );
});

export default ModalRegistration;