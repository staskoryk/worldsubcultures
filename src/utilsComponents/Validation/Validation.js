import React from 'react';
import style from './Validation.module.css'

const Validation = ({children}) => {
    return (
        <div className={style.validation}>
            {children}
        </div>
    );
};

export default Validation;