import React from 'react';
import style from './ItemList.module.css'

const ItemList = ({children}) => {
    return (
        <div className={style.listBlock}>
            {children}
        </div>
    );
};

export default ItemList;