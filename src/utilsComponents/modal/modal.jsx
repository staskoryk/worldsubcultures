import React, {useContext} from 'react';
import style from './modal.module.css'
import {Context} from "../../index";
import {observer} from "mobx-react-lite";


const Modal = observer(({children}) => {
    const {modal} = useContext(Context)
    return (
        <div className={modal.isModal ? `${style.modal} ${style.active}` : `${style.modal}`} onClick={() => modal.setIsModal(false)}>
            <div className={modal.isModal ? `${style.modalContent} ${style.active}` : `${style.modalContent}`} onClick={e => e.stopPropagation()}>
                {children}
            </div>
        </div>
    );
});

export default Modal;