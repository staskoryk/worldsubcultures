import {makeAutoObservable} from "mobx";

export default class UserStore {
    constructor() {
        this._isAuth = false
        this._user = {}
        this._basket = []
        this._favoriteClothes = []
        makeAutoObservable(this)
    }

    setIsAuth(bool) {
        this._isAuth = bool
    }


    setUserAuth(user) {
        this._user = user
    }

    setBasketItems(basket) {
        this._basket = basket
    }

    setFavoriteClothes(clothes) {
        this._favoriteClothes = clothes
    }

    get isAuth() {
        return this._isAuth
    }



    get user() {
        return this._user
    }

    get basket() {
        return this._basket
    }

    get favoriteClothes() {
        return this._favoriteClothes
    }

}