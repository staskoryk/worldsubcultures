import {makeAutoObservable} from "mobx";

export default class ModalStore {
    constructor() {
        this._isModal = false
        this._isModalRegistration = false
        makeAutoObservable(this)
    }

    setIsModal(bool) {
        this._isModal = bool
    }

    setIsModalRegistration(bool) {
        this._isModalRegistration = bool
    }


    get isModal() {
        return this._isModal
    }

    get isRegistrationModal() {
        return this._isModalRegistration
    }

}