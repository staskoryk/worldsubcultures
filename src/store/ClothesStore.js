import {makeAutoObservable} from "mobx";

export default class ClothesStore {
    constructor() {
        this._clothes = []
        this._sex = ''
        this._type = ''
        this._name = ''

        makeAutoObservable(this)
    }

    setClothes(clothes) {
        this._clothes = clothes
    }

    setSex(sex) {
        this._sex = sex
    }

    setType(type) {
        this._type = type
    }

    setName(name) {
        this._name = name
    }


    get sex() {
        return this._sex
    }

    get type() {
        return this._type
    }

    get name() {
        return this._name
    }
    get clothes() {
        return this._clothes
    }

}